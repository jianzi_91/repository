<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Faker;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class KeyPairTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_list()
    {
        $response = $this->getJson('/api/object/get_all_records');

        $response->assertStatus(200);
    }

    public function test_create_key_without_key()
    {
        $response = $this->postJson('/api/object');

        $response->assertStatus(422);
    }

    public function test_create_key_without_value()
    {
        $response = $this->postJson('api/object', [
            'key' => null,
        ]);

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'key' => null,
                ],
            ]);
    }

    public function test_retrieve_key_without_key()
    {
        $response = $this->getJson('api/object/');

        $response->assertStatus(405);
    }

    public function test_retrieve_key_with_non_existence_key()
    {
        $faker = Faker\Factory::create();

        $response = $this->getJson('api/object/' . $faker->word);

        $response->assertStatus(404)
            ->assertJson([
                'data' => null
            ]);
    }
}
