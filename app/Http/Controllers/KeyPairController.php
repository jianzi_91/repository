<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contracts\KeyPairContract;
use App\Http\Requests\ShowKPRequest;
use App\Http\Requests\StoreKPRequest;
use App\Http\Resources\KeyPairResource;
use App\Http\Resources\KeyPairCollection;
use Illuminate\Support\Facades\DB;

class KeyPairController extends Controller
{
    /**
     * Class constructor
     */
    public function __construct(KeyPairContract $keyPairContract)
    {
        $this->keyPairRepo = $keyPairContract;
    }

    /**
     * To store a key pair into database
     * 
     * @param StoreKPRequest
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            
            json_decode($request->getContent());
            if (json_last_error() != JSON_ERROR_NONE)
            {
                throw new \Exception('Malformed JSON.');
            }
            $keyPairs = $request->all();

            if (empty($keyPairs)) {
                return response()->json([
                    'msg' => 'No key pair submitted',
                    'data' => null,
                    'created_at' => now(),
                ], 422);
            }

            foreach ($keyPairs as $key => $value) {
                $keyPair = $this->keyPairRepo->storeKeyPair($key, $value);
            }

            DB::commit();
        } catch (\Exception $e) {
            return response()->json([
                'msg'  => $e->getMessage(),
                'data' => null,
            ]);
        }

        return response()->json([
            'msg'  => 'Key pair created',
            'data' => $keyPairs,
            'created_at' => time(),
        ]);
    }

    /**
     * Display a key pair
     * @param Request $request
     * @param string $key
     * @return Json
     */
    public function show(ShowKPRequest $request, $key)
    {
        $data = $request->only('unix_timestamp');

        $keyExistence = $this->keyPairRepo->checkKeyExistence($key);

        if (!$keyExistence) {
            return response()->json([
                'msg'  => 'Key does not exist',
                'data' => null,
            ], 404);
        }

        if (empty($data['unix_timestamp'])) {
            $data['unix_timestamp'] = null;
        }

        $history = $this->keyPairRepo->getKeyPair($key, $data['unix_timestamp']);

        return response()->json([
            'msg'  => 'Key pair retrived',
            'data' => [
                'key'   => $history->key,
                'value' => $history->value,
            ]
        ]);
    }

    /**
     * List all key pair
     * @param Request $request
     * @return Json
     */
    public function list(Request $request)
    {
        $keyPairs = $this->keyPairRepo->getAllKeyPairs();

        return KeyPairCollection::make($keyPairs);
    }
}
