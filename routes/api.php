<?php

use App\Http\Controllers\KeyPairController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/object/get_all_records', [KeyPairController::class, 'list']);
Route::get('object/{key}', [KeyPairController::class, 'show']);
Route::post('object', [KeyPairController::class, 'store']);