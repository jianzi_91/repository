<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeyPairHistory extends Model
{
    use HasFactory;

    protected $table   = "key_pair_histories";
    protected $guarded = [];

    public function getKeyAttribute()
    {
        return $this->keyPair->key;
    }

    public function keyPair()
    {
        return $this->belongsTo(KeyPair::class, 'key_pair_id');
    }
}
