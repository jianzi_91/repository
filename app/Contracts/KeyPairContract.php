<?php

namespace App\Contracts;

use App\Models\KeyPair;
use App\Models\KeyPairHistory;

interface KeyPairContract 
{
    /**
     * To get a key => value from database
     * 
     * @param $key
     * @return KeyPair
     */
    public function getKeyPair($key, $datetime = null) : KeyPairHistory;

    /**
     * Store a key and its value
     * 
     * @param $key
     * @param $value
     */
    public function storeKeyPair($key, $value) : KeyPair;

    /**
     * Get all keys and its past values
     */
    public function getAllKeyPairs();

    /**
     * Check the existence of key 
     * @param string $key
     * @return Boolean
     */
    public function checkKeyExistence($key) : bool;
}

