<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeyPair extends Model
{
    use HasFactory;

    protected $table   = "key_pairs";
    protected $guarded = [];

    /**
     * Relationship with history model
     */
    public function histories()
    {
        return $this->hasMany(KeyPairHistory::class, 'key_pair_id');
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        // On creation or update a key pair, create a history of it
        static::created(function ($model) {
            $model->histories()->create([
                'key_pair_id'    => $model->id,
                'value'          => $model->value,
                'unix_timestamp' => time(),
            ]);
        });

        static::updated(function ($model) {
            $model->histories()->create([
                'key_pair_id'    => $model->id,
                'value'          => $model->value,
                'unix_timestamp' => time(),
            ]);
        });
    }
}
