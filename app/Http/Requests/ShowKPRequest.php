<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShowKPRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'unix_timestamp' => 'nullable|integer'
        ];
    }

    public function messages()
    {
        return [
            'unix_timestamp.integer' => 'Not valid format of unix timestamp.',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
