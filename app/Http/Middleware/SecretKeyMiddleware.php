<?php

namespace App\Http\Middleware;

use Closure;

class SecretKeyMiddleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        if ($request->method() == 'GET') {
            if (empty($request->secret) || $request->secret != '123') {
                return response()->json([
                    'data' => null,
                    'msg' => 'unauthorized',
                ], 401);
            }
        }

        return $next($request);
    }
}
