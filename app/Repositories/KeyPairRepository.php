<?php

namespace App\Repositories;

use App\Models\KeyPair;
use App\Models\KeyPairHistory;
use App\Contracts\KeyPairContract;
use phpDocumentor\Reflection\Types\Boolean;

class KeyPairRepository implements KeyPairContract
{
    public function getKeyPair($key, $unix = null) : KeyPairHistory
    {
        $keyPair = KeyPair::where('key', $key)->first();

        if (!empty($unix)) {
            $history = KeyPairHistory::where('key_pair_id', $keyPair->id)
                ->where('unix_timestamp', $unix)
                ->first();

            //If there is no unix match the exact we need to get the closest before one
            if (empty($history)) {
                $history = KeyPairHistory::where('key_pair_id', $keyPair->id)
                    ->where('unix_timestamp', '<', $unix)
                    ->orderBy('unix_timestamp', 'DESC')
                    ->first();
            }
        } else {
            $history = KeyPairHistory::where('key_pair_id', $keyPair->id)
            ->orderBy('created_at', 'DESC')
            ->first();
        }

        return $history;
    }

    public function storeKeyPair($key, $value): KeyPair
    {
        $keyPair = KeyPair::where('key', $key)->first();

        //  If keypair exist
        if ($keyPair) {
            // If a different value is given update it
            if ($keyPair->value != $value) {
                $keyPair->update([
                    'value' => $value,
                ]);
            }
        } else {
            $keyPair = KeyPair::create([
                'key'   => $key,
                'value' => $value,
            ]);
        }

        return $keyPair;
    }

    public function getAllKeyPairs()
    {
        return KeyPair::all();
    }

    public function checkKeyExistence($key) : bool
    {
        return KeyPair::where('key', $key)
            ->exists();
    }
}

