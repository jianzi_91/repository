<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'universal_api');

// Project repository
set('repository', 'git@gitlab.com:jianzi_91/repository.git');

set('composer_options', 'install --verbose --prefer-dist --no-progress --no-interaction --optimize-autoloader');

// [Optional] Allocate tty for git clone. Default value is false.
// set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts

host('174.138.19.53')
    ->user('deployer')
    ->identityFile('~/.ssh/deployerkey')
    ->set('deploy_path', '/var/www/html/universal-api');    
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// task(
//     'artisan:test', 
//     function() {
//         run('{{bin/php}} {{release_path}}/artisan test');
//     }
// );

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');

// after('artisan:migrate', 'artisan:test');

